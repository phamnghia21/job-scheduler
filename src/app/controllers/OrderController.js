import Queue from '../lib/Queue';

export default {
    async store(req, res) {
        const query = req.query;

        await Queue.add('GetOrders', { query });
        return res.json("Ok");
    }
};
