import axios from 'axios';
import moment from 'moment';

export default {
    key: 'GetOrders',
    options: {
        repeat: {
            every: Number(process.env.X * 60)
        }
    },
    async handle({ }) {
        var from = moment().add(1, 'days').toISOString();
        var to = moment().add(1, 'days').add(30, 'minutes').toISOString();
        var orders = await axios.get(
            `${process.env.OPS_API_URL}/orders?from=${from}&to=${to}`,
            { headers: { 'Authorization': `Bearer ${process.env.OPS_SVC_TOKEN}` } })
            .then(_response => _response.data.data);

        var order_codes = orders.map(order => order.code);
        console.log(order_codes);
    }
}
